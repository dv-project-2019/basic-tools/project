async function loadDefaultAnime() {
    let defaultAnime = "Tonari no Totoro";
    // let annimeName = document.getElementById('anime-input');
    // annimeName.defaultValue = defaultAnime;
    let response = await fetch('https://api.jikan.moe/v3/search/anime?q=' + defaultAnime)
    let data = await response.json();
    console.log(response);
    return data;

}

async function searchAnimeByName() {
    let annimeInput = document.getElementById('anime-input').value;
    console.log(annimeInput);
    if (annimeInput != '' && annimeInput != null) {
        let response = await fetch('https://api.jikan.moe/v3/search/anime?q=' + annimeInput)
        let data = await response.json();
        console.log(response);
        return data;
    }
}

function createResultTable(data) {
    //let annimeName = document.getElementById('anime-input').value;
    let animeResult = document.getElementById('anime-result');
    animeResult.innerText = "";

    let tableResponsive = document.createElement('div');
    tableResponsive.setAttribute('class', 'table-responsive-sm');
    animeResult.appendChild(tableResponsive);

    let tableNode = document.createElement('table');
    tableNode.setAttribute('class', 'table');
    tableNode.style.fontFamily = 'Gudea', 'sans-serif';
    tableResponsive.appendChild(tableNode);

    //create the table
    let tableHeadNode = document.createElement('thead');
    tableHeadNode.setAttribute('class', 'thead-light');
    tableHeadNode.style.textAlign = 'center';
    tableNode.appendChild(tableHeadNode);

    var tableRowNode = document.createElement('tr');
    tableHeadNode.appendChild(tableRowNode);

    var tableHeaderNode = document.createElement('th');
    tableHeaderNode.setAttribute('scope', 'col');
    tableHeaderNode.innerText = 'No.';
    tableHeadNode.appendChild(tableHeaderNode);

    var tableHeaderNode = document.createElement('th');
    tableHeaderNode.setAttribute('scope', 'col');
    tableHeaderNode.innerText = 'Id';
    tableHeadNode.appendChild(tableHeaderNode);

    tableHeaderNode = document.createElement('th');
    tableHeaderNode.setAttribute('scope', 'col');
    tableHeaderNode.innerText = 'Anime name';
    tableHeadNode.appendChild(tableHeaderNode);

    tableHeaderNode = document.createElement('th');
    tableHeaderNode.setAttribute('scope', 'col');
    tableHeaderNode.innerText = 'Synopsis';
    tableHeadNode.appendChild(tableHeaderNode);

    tableHeaderNode = document.createElement('th');
    tableHeaderNode.setAttribute('scope', 'col');
    tableHeaderNode.innerText = 'Type';
    tableHeadNode.appendChild(tableHeaderNode);

    tableHeaderNode = document.createElement('th');
    tableHeaderNode.setAttribute('scope', 'col');
    tableHeaderNode.innerText = 'Score';
    tableHeadNode.appendChild(tableHeaderNode);

    tableHeaderNode = document.createElement('th');
    tableHeaderNode.setAttribute('scope', 'col');
    tableHeaderNode.innerText = 'Image';
    tableHeadNode.appendChild(tableHeaderNode);

    console.log(data);
    data.then((json) => {
        for (let i = 0; i < 10; i++) {
            console.log(json.results[i]);
            var currentData = json.results[i];

            var dataRow = document.createElement('tr');
            dataRow.style.margin = '5px';
            tableNode.appendChild(dataRow);

            var dataFirstColumnNode = document.createElement('th');
            dataFirstColumnNode.setAttribute('scope', 'row');
            dataFirstColumnNode.innerText = (i+1);
            dataFirstColumnNode.style.textAlign = 'center';
            dataRow.appendChild(dataFirstColumnNode);

            var columnNode = null;
            columnNode = document.createElement('td');
            columnNode.innerText = currentData['mal_id'];
            columnNode.style.textAlign = 'center';
            dataRow.appendChild(columnNode);

            columnNode = document.createElement('td');
            columnNode.innerText = currentData['title'];
            columnNode.style.textAlign = 'center';
            dataRow.appendChild(columnNode);

            columnNode = document.createElement('td');
            columnNode.innerText = currentData['synopsis'];
            dataRow.appendChild(columnNode);

            columnNode = document.createElement('td');
            columnNode.innerText = currentData['type'];
            columnNode.style.textAlign = 'center';
            dataRow.appendChild(columnNode);

            columnNode = document.createElement('td');
            columnNode.innerText = currentData['score'];
            columnNode.style.textAlign = 'center';
            dataRow.appendChild(columnNode);

            columnNode = document.createElement('td');
            var imageNode = document.createElement('img');
            imageNode.setAttribute('src', currentData['image_url']);
            columnNode.style.textAlign = 'center';
            imageNode.style.width = '200px';
            imageNode.style.height = '200px';
            dataRow.appendChild(imageNode);
        }

    })
}